﻿using System;
using BusinessLogic.Models;

namespace BusinessLogic.Services
{
    public class UserService : ApplicationService
    {
        private User _currentUser;

        public long CurrentUserId
        {
            get { return _currentUser.Id; }
        }

        public string CurrentUserName
        {
            get { return _currentUser.UserName; }
        }

        public string CurrentUserGroup
        {
            get { return _currentUser.Group.ToString(); }
        }

        public bool Authenticate(string userName, string password)
        {
            var userGateway = GetUserGateway();
            var dataReader = userGateway.FindForUserName(userName);
            if (!dataReader.Read())
            {
                return false;
            }

            var user = new User
            {
                Id = Convert.ToInt64(dataReader["Id"]),
                UserName = dataReader["UserName"].ToString(),
                Password = dataReader["Password"].ToString(),
                Group = (Group) Enum.Parse(typeof(Group), dataReader["Group"].ToString()),
            };

            if (BCrypt.Net.BCrypt.Verify(password, user.Password))
            {
                _currentUser = user;
                return true;
            }

            return false;
        }

        public bool Register(string userName, string password, bool isAdmin)
        {
            var userGateway = GetUserGateway();
            var dataReader = userGateway.FindForUserName(userName);
            if (dataReader.Read())
            {
                return false;
            }

            var passwordHash = BCrypt.Net.BCrypt.HashPassword(password);
            var group = isAdmin ? Group.Administrator : Group.User;

            userGateway.Insert(userName, passwordHash, group);

            return true;
        }
    }
}
