﻿using BusinessLogic.Gateways;

namespace BusinessLogic.Services
{
    public abstract class ApplicationService
    {
        private readonly IUserGateway _userGateway = new UserTableDataGateway();
        private readonly IFileGateway _fileGateway = new FileTableDataGateway();

        protected IUserGateway GetUserGateway()
        {
            return _userGateway;
        }

        protected IFileGateway GetFileGateway()
        {
            return _fileGateway;
        }
    }
}
