﻿using BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Services
{
    public class FileService : ApplicationService
    {
        private File _selectedFile;
        private List<File> _allFiles;

        public string SelectedFileName
        {
            get { return _selectedFile.FileName; }
        }

        public string SelectedFileSize
        {
            get { return string.Format("{0:F} Мб.", _selectedFile.Size / (1024.0 * 1024.0)); }
        }

        public string SelectedFileRights
        {
            get { return _selectedFile.Group == Group.User ? "Личный" : "Общий"; }
        }

        public string SelectedFileUserName
        {
            get { return _selectedFile.UserName; }
        }

        public void Save(string userName, string path, bool isOwn)
        {
            var filename = path.Split('\\').Last();
            var location = string.Format("{0}\\{1}", userName, filename);
            var size = new System.IO.FileInfo(path).Length;

            if (!System.IO.Directory.Exists(userName))
            {
                System.IO.Directory.CreateDirectory(userName);
            }

            if (!System.IO.File.Exists(location))
            {
                System.IO.File.Copy(path, location);
            }
                     
            var file = new File
            {
                FileName = filename,
                Location = location,
                Size = size,
                UserName = userName,
                Group = isOwn ? Group.User : Group.None,
            };

            var fileGateway = GetFileGateway();
            file.Id = fileGateway.Insert(file.FileName, file.Location, file.Size, file.UserName, file.Group);

            _allFiles.Add(file);
        }

        public void Extract(string path)
        {
            System.IO.File.Copy(_selectedFile.Location, string.Format("{0}\\{1}", path, _selectedFile.FileName));
        }

        public void SelectById(long id)
        {
            _selectedFile = _allFiles.Find(m => m.Id == id);

            if (_selectedFile == null)
            {
                _selectedFile = GetFile(id);
            }
        }

        private File GetFile(long id)
        {
            var fileGateway = GetFileGateway();
            var dataReader = fileGateway.Find(id);
            return new File
            {
                Id = (long)dataReader["Id"],
                FileName = (string)dataReader["FileName"],
                Location = (string)dataReader["Location"],
                Size = (long)dataReader["Size"],
                UserName = (string)dataReader["UserName"],
                Group = (Group)dataReader["Group"],
            };
        }

        public IEnumerable<Tuple<long, string>> FindByFileName(string fileName)
        {
            var upperName = fileName.ToUpper();
            return _allFiles.Select(file => new Tuple<long, string>(file.Id, file.FileName))
                .Where(m => m.Item2.ToUpper().Contains(upperName));
        }

        public IEnumerable<Tuple<long, string>> FindForUserName(string userName)
        {
            if (_allFiles != null)
            {
                return _allFiles.Select(file => new Tuple<long, string>(file.Id, file.FileName));
            }

            var userGateway = GetUserGateway();
            var dataReader = userGateway.FindForUserName(userName);
            dataReader.Read();
            var userGroup = (Group)Enum.Parse(typeof(Group), dataReader["Group"].ToString());

            _allFiles = userGroup == Group.Administrator ? GetFiles() : GetFilesForUser(userName);

            return _allFiles.Select(file => new Tuple<long, string>(file.Id, file.FileName));
        }

        private List<File> GetFilesForUser(string userName)
        {
            return GetFiles().Where(file => file.Group == Group.None || file.UserName == userName).ToList();
        }

        private List<File> GetFiles()
        {
            var fileGateway = GetFileGateway();
            var dataReader = fileGateway.All();
            var files = new List<File>();
            while (dataReader.Read())
            {
                var id = dataReader["Id"];
                var fileName = dataReader["FileName"];
                var location = dataReader["Location"];
                var size = dataReader["Size"];
                var userName = dataReader["UserName"];
                var group = dataReader["Group"];

                files.Add(new File
                {
                    Id = Convert.ToInt64(id),
                    FileName = fileName.ToString(),
                    Location = location.ToString(),
                    Size = Convert.ToInt64(size),
                    UserName = userName.ToString(),
                    Group = (Group)Enum.Parse(typeof(Group), group.ToString()),
                });
            }
            return files;
        }

        public void Delete()
        {
            _allFiles.Remove(_selectedFile);

            var fileGateway = GetFileGateway();
            fileGateway.Delete(_selectedFile.Id);
            System.IO.File.Delete(_selectedFile.Location);
        }
    }
}
