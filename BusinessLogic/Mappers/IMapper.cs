﻿using System.Collections.Generic;

namespace BusinessLogic.Mappers
{
    public interface IMapper<TModel>
    {
        long Add(TModel model);
        TModel FindById(long id);
        void Delete(TModel model);
        void Update(TModel model);
        IEnumerable<TModel> All();
    }
}
