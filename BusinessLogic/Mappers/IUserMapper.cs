﻿using BusinessLogic.Models;

namespace BusinessLogic.Mappers
{
    public interface IUserMapper : IMapper<User>
    {
        User FindByUserName(string userName);
    }
}
