﻿using System.Collections.Generic;
using BusinessLogic.Models;
using System.Data;

namespace BusinessLogic.Mappers
{
    public interface IFileMapper : IMapper<File>
    {
        IEnumerable<File> FindForUserName(string userName);
    }
}
