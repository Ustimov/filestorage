﻿using BusinessLogic.Factories;
using BusinessLogic.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Mappers
{
    public class OrmLiteFileMapper : IFileMapper
    {
        private readonly IConnectionFactory _connectionFactory;

        public OrmLiteFileMapper()
        {
            _connectionFactory = OrmLiteSqliteConnectionFactory.GetInstance();

            using (var connection = _connectionFactory.GetConnection())
            {
                connection.CreateTableIfNotExists<File>();
            }
        }

        public long Add(File file)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Insert(file);
                return connection.LastInsertId();
            }
        }

        public File FindById(long id)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                return connection.Select<File>().FirstOrDefault(file => file.Id == id);
            }
        }

        public IEnumerable<File> FindForUserName(string userName)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                return connection.Select<File>().Where(file => file.Group == Group.None || file.UserName == userName);
            }
        }

        public void Delete(File file)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Delete(file);
            }
        }

        public void Update(File file)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Update(file);
            }
        }

        public IEnumerable<File> All()
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                return connection.Select<File>();
            }
        }
    }
}
