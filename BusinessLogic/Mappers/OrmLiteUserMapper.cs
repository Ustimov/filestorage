﻿using BusinessLogic.Factories;
using BusinessLogic.Models;
using ServiceStack.OrmLite;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Mappers
{
    internal class OrmLiteUserMapper : IUserMapper
    {
        private readonly IConnectionFactory _connectionFactory;

        public OrmLiteUserMapper()
        {
            _connectionFactory = OrmLiteSqliteConnectionFactory.GetInstance();

            using (var connection = _connectionFactory.GetConnection())
            {
                connection.CreateTableIfNotExists<User>();
            }
        }

        public long Add(User user)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Insert(user);
                return connection.LastInsertId();
            }
        }

        public User FindById(long id)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                return connection.Select<User>().FirstOrDefault(user => user.Id == id);
            }
        }

        public User FindByUserName(string userName)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                return connection.Select<User>().FirstOrDefault(user => user.UserName == userName);
            }
        }

        public void Delete(User user)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Delete(user);
            }
        }

        public void Update(User user)
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                connection.Update(user);
            }
        }

        public IEnumerable<User> All()
        {
            using (var connection = _connectionFactory.GetConnection())
            {
                return connection.Select<User>();
            }
        }
    }
}
