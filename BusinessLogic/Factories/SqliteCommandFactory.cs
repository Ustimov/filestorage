﻿using System.Data;
using System.Data.SQLite;

namespace BusinessLogic.Factories
{
    public class SqliteCommandFactory : ICommandFactory
    {
        private static readonly ICommandFactory _instance = new SqliteCommandFactory();

        private readonly string connectionString = "Data Source=archive.db;Version=3;";

        protected SqliteCommandFactory()
        {

        }

        public static ICommandFactory GetInstance()
        {
            return _instance;
        }

        public IDbCommand GetCommand(string commandText)
        {
            var connection = new SQLiteConnection(connectionString);
            connection.Open();
            var command = new SQLiteCommand(commandText, connection);
            return command;
        }
    }
}
