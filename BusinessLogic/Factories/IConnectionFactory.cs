﻿using System.Data;

namespace BusinessLogic.Factories
{
    internal interface IConnectionFactory
    {
        IDbConnection GetConnection();
    }
}
