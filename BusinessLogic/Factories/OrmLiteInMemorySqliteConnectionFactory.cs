﻿using ServiceStack.OrmLite;
using System.Data;

namespace BusinessLogic.Factories
{
    internal class OrmLiteInMemorySqliteConnectionFactory : IConnectionFactory
    { 
        private static readonly IConnectionFactory _instance = new OrmLiteInMemorySqliteConnectionFactory();

        private readonly OrmLiteConnectionFactory _connectionFactory = 
            new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider);

        protected OrmLiteInMemorySqliteConnectionFactory()
        {

        }

        public IDbConnection GetConnection()
        {
            return _connectionFactory.Open();
        }

        public static IConnectionFactory GetInstance()
        {
            return _instance;
        }
    }
}
