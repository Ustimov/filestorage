﻿using System.Data;

namespace BusinessLogic.Factories
{
    public interface ICommandFactory
    {
        IDbCommand GetCommand(string commandText);
    }
}
