﻿using ServiceStack.OrmLite;
using System.Data;

namespace BusinessLogic.Factories
{
    internal class OrmLiteSqliteConnectionFactory : IConnectionFactory
    {
        private static readonly IConnectionFactory _instance = new OrmLiteSqliteConnectionFactory();

        private readonly OrmLiteConnectionFactory _connectionFactory = 
            new OrmLiteConnectionFactory("archive.db", SqliteDialect.Provider);

        protected OrmLiteSqliteConnectionFactory()
        {

        }

        public IDbConnection GetConnection()
        {
            return _connectionFactory.Open();
        }

        public static IConnectionFactory GetInstance()
        {
            return _instance;
        }
    }
}
