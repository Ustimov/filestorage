﻿using System.Data;
using BusinessLogic.Factories;
using BusinessLogic.Models;

namespace BusinessLogic.Gateways
{
    public class UserTableDataGateway : IUserGateway
    {
        private readonly ICommandFactory _commandFactory;

        public UserTableDataGateway()
        {
            _commandFactory = SqliteCommandFactory.GetInstance();
        }

        public long Insert(string userName, string password, Group group)
        {
            var command = _commandFactory.GetCommand($"insert into User (UserName, Password, 'Group') " +
                                                     $"values ('{userName}', '{password}', '{group}')");
            return command.ExecuteNonQuery();
        }

        public IDataReader Find(long id)
        {
            var command = _commandFactory.GetCommand($"select * from User where Id={id}");
            return command.ExecuteReader();
        }

        public IDataReader FindForUserName(string userName)
        {
            var command = _commandFactory.GetCommand($"select * from User where UserName='{userName}'");
            return command.ExecuteReader();
        }

        public void Update(long id, string userName, string password, Group group)
        {
            var command = _commandFactory.GetCommand($"update User set UserName='{userName}', " +
                                                     $"Password='{password}', Group='{group}' where Id={id}");
            command.ExecuteNonQuery();
        }

        public void Delete(long id)
        {
            var commmand = _commandFactory.GetCommand($"delete from User where Id={id}");
            commmand.ExecuteNonQuery();
        }

        public IDataReader All()
        {
            var command = _commandFactory.GetCommand("select * from User");
            return command.ExecuteReader();
        }
    }
}
