﻿using BusinessLogic.Models;
using System.Data;

namespace BusinessLogic.Gateways
{
    public interface IUserGateway
    {
        long Insert(string userName, string password, Group group);
        IDataReader Find(long id);
        IDataReader FindForUserName(string userName);
        void Update(long id, string userName, string password, Group group);
        void Delete(long id);
        IDataReader All();
    }
}
