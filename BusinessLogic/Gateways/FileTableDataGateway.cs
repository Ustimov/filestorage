﻿using System.Data;
using BusinessLogic.Factories;
using BusinessLogic.Models;

namespace BusinessLogic.Gateways
{
    public class FileTableDataGateway : IFileGateway
    {
        private readonly ICommandFactory _commandFactory;

        public FileTableDataGateway()
        {
            _commandFactory = SqliteCommandFactory.GetInstance();
        }

        public long Insert(string fileName, string location, long size, string userName, Group group)
        {
            var sql = $"insert into File (FileName, Location, Size, UserName, 'Group') " +
                      $"values ('{fileName}', '{location}', {size}, '{userName}', '{group}')";
            var command = _commandFactory.GetCommand(sql);
            return command.ExecuteNonQuery();
        }

        public IDataReader Find(long id)
        {
            var command = _commandFactory.GetCommand($"select * from File where Id={id}");
            return command.ExecuteReader();
        }

        public void Update(long id, string fileName, string location, long size, string userName, Group group)
        {
            var command = _commandFactory.GetCommand($"update File set FileName={fileName}, Location={location}, " +
                                                     $"Size={size}, UserName={userName}, Group={group} where Id={id}");
            command.ExecuteNonQuery();
        }

        public void Delete(long id)
        {
            var command = _commandFactory.GetCommand($"delete from File where Id={id}");
            command.ExecuteNonQuery();
        }

        public IDataReader All()
        {
            var command = _commandFactory.GetCommand("select * from File");
            return command.ExecuteReader();
        }
    }
}
