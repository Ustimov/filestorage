﻿using BusinessLogic.Models;
using System.Data;

namespace BusinessLogic.Gateways
{
    public interface IFileGateway
    {
        long Insert(string fileName, string location, long size, string userName, Group group);
        IDataReader Find(long id);
        void Update(long id, string fileName, string location, long size, string userName, Group group);
        void Delete(long id);
        IDataReader All();
    }
}
