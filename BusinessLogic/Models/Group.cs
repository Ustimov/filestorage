﻿namespace BusinessLogic.Models
{
    public enum Group
    {
        None,
        Administrator,
        User,
    }
}
