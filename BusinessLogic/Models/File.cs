﻿using ServiceStack.DataAnnotations;

namespace BusinessLogic.Models
{
    public class File
    {
        [AutoIncrement]
        public long Id { get; set; }

        public string FileName { get; set; }

        public string Location { get; set; }

        public long Size { get; set; }

        public string UserName { get; set; }

        public Group Group { get; set; }
    }
}
