﻿using ServiceStack.DataAnnotations;

namespace BusinessLogic.Models
{
    public class User
    {
        [AutoIncrement]
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public Group Group { get; set; }
    }
}
