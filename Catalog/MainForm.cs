﻿using BusinessLogic.Services;
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Windows.Forms;

namespace Catalog
{
    public partial class MainForm : Form
    {
        private readonly Timer _timer = new Timer { Interval = 1000 };
        private bool _isSearchDispatched = false;

        private readonly FileService _fileService = new FileService();
        private readonly UserService _userService = new UserService();

        public MainForm()
        {
            InitializeComponent();

            fileTreeView.NodeMouseClick += TreeViewNodeMouseClick;
            searchTextBox.TextChanged += SearchTextBoxTextChanged;

            _timer.Tick += TimerTick;

            this.Load += MainFormLoad;
        }

        private void TimerTick(object sender, EventArgs e)
        {
            _timer.Stop();
            UpdateTreeView(_fileService.FindByFileName(searchTextBox.Text));
            _isSearchDispatched = false;
        }

        private void SearchTextBoxTextChanged(object sender, EventArgs e)
        {
            if (_isSearchDispatched)
            {
                return;
            }

            _isSearchDispatched = true;

            _timer.Start();
        }

        private void TreeViewNodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var id = Convert.ToInt64(e.Node.Name);
            SelectFileWithId(id);
        }

        private void SelectFileWithId(long id)
        {
            _fileService.SelectById(id);
            fileNameLabel.Text = _fileService.SelectedFileName;
            fileSizeLabel.Text = _fileService.SelectedFileSize;
            fileAccessRightsLabel.Text = _fileService.SelectedFileRights;
            fileUserNameLabel.Text = _fileService.SelectedFileUserName;

            if (_fileService.SelectedFileUserName == _userService.CurrentUserName
                || _userService.CurrentUserGroup == "Administrator")
            {
                deleteButton.Visible = true;
            }
            else
            {
                deleteButton.Visible = false;
            }
        }

        private void MainFormLoad(object sender, EventArgs e)
        {
            var loginForm = new LoginForm();
            loginForm.RegisterButton.Click += RegisterButtonClick;
            var authenticated = false;
            while (!authenticated)
            {
                if (loginForm.ShowDialog() != DialogResult.OK)
                {
                    this.Close();
                    return;
                }

                if (string.IsNullOrEmpty(loginForm.Username) || string.IsNullOrEmpty(loginForm.Password))
                {
                    MessageBox.Show("Поля ввода не должны быть пустыми");
                    continue;
                }

                authenticated = _userService.Authenticate(loginForm.Username, loginForm.Password);
                if (!authenticated)
                {
                    MessageBox.Show("Неверные учётные данные");
                }
            }

            userToolStripStatusLabel.Text = string.Format("{0} ({1})", _userService.CurrentUserName, 
                _userService.CurrentUserGroup);

            UpdateTreeView(_fileService.FindForUserName(_userService.CurrentUserName));
        }

        private void RegisterButtonClick(object sender, EventArgs e)
        {
            var registrationForm = new RegistrationForm();
            var registered = false;
            while (!registered)
            {
                if (registrationForm.ShowDialog() != DialogResult.OK)
                {
                    break;
                }

                if (string.IsNullOrEmpty(registrationForm.Username) || string.IsNullOrEmpty(registrationForm.Password)
                    || string.IsNullOrEmpty(registrationForm.ConfirmPassword))
                {
                    MessageBox.Show("Поля ввода не должны быть пустыми");
                    continue;
                }

                if (registrationForm.Password != registrationForm.ConfirmPassword)
                {
                    MessageBox.Show("Введённые пароли не совпадают");
                    continue;
                }

                registered = _userService.Register(registrationForm.Username, registrationForm.Password, registrationForm.isAdmin);
                if (!registered)
                {
                    MessageBox.Show("Пользователь с данным именем уже существует");
                }
            }
        }

        private void UpdateTreeView(IEnumerable<Tuple<long, string>> files)
        {
            fileTreeView.Nodes.Clear();

            foreach (var file in files)
            {
                fileTreeView.Nodes.Add(file.Item1.ToString(), file.Item2);
            }

            if (files.Any())
            {
                SelectFileWithId(files.First().Item1);
            }
        }

        private void AddFileToolStripMenuItemClick(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            var accessRightForm = new AccessRightForm();
            accessRightForm.ShowDialog();

            _fileService.Save(_userService.CurrentUserName, openFileDialog.FileName, accessRightForm.IsOwn);

            UpdateTreeView(_fileService.FindForUserName(_userService.CurrentUserName));
        }

        private void ExtractButtonClick(object sender, EventArgs e)
        {
            var folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            _fileService.Extract(folderBrowserDialog.SelectedPath);

            MessageBox.Show("Файл успешно извлечён");
        }

        private void DeleteButtonClick(object sender, EventArgs e)
        {
            _fileService.Delete();
            UpdateTreeView(_fileService.FindForUserName(_userService.CurrentUserName));
        }

        private void ExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }
    }
}
