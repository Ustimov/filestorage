﻿using System;
using System.Windows.Forms;

namespace Catalog
{
    public partial class LoginForm : Form
    {
        public string Username
        {
            get { return usernameTextBox.Text; }
        }

        public string Password
        {
            get { return passwordTextBox.Text; }
        }

        public Button RegisterButton
        {
            get { return registerButton; }
        }

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginButtonClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
