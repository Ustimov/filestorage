﻿using System;
using System.Windows.Forms;

namespace Catalog
{
    public partial class RegistrationForm : Form
    {
        public string Username
        {
            get { return usernameTextBox.Text; }
        }

        public string Password
        {
            get { return passwordTextBox.Text; }
        }

        public string ConfirmPassword
        {
            get { return confirmPasswordTextBox.Text; }
        }

        public bool isAdmin
        {
            get { return isAdminCheckBox.Checked; }
        }

        public RegistrationForm()
        {
            InitializeComponent();
        }

        private void RegisterButtonClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
