﻿using System;
using System.Windows.Forms;

namespace Catalog
{
    public partial class AccessRightForm : Form
    {
        private bool ignoreCheckedChanged;

        public bool IsOwn
        {
            get { return ownRadioButton.Checked; }
        }

        public AccessRightForm()
        {
            InitializeComponent();

            ownRadioButton.CheckedChanged += OwnRadioButtonCheckedChanged;
            allRadioButton.CheckedChanged += AllRadioButtonCheckedChanged;
        }

        private void AllRadioButtonCheckedChanged(object sender, EventArgs e)
        {
            if (ignoreCheckedChanged)
            {
                return;
            }

            ignoreCheckedChanged = true;

            if (allRadioButton.Checked)
            {
                ownRadioButton.Checked = false;
            }
            else
            {
                ownRadioButton.Checked = true;
            }
        }

        private void OwnRadioButtonCheckedChanged(object sender, EventArgs e)
        {
            if (ignoreCheckedChanged)
            {
                return;
            }

            ignoreCheckedChanged = true;

            if (ownRadioButton.Checked)
            {
                allRadioButton.Checked = false;
            }
            else
            {
                allRadioButton.Checked = true;
            }
        }

        private void ApplyButtonClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
